import { userConstants } from '../constants/users.constants'

export const usersActions = {
    getAllUser
}

function getAllUser() {
    // peticion a api o cualquier origen de datos

    return dispatch => {
        //dispara la accion de la peticion
        dispatch(request())

        //peticion http get
        const users = [
            {name:'Daniel', apellido: 'El travieso'},
            {name:'Pastor', apellido: 'Aleman'},
            {name:'Ivan', apellido: 'El terrible'},
            {name:'Carlos', apellido: 'V'}
        ]

        //dispara la accion de exito
        dispatch(success(users))

    }

    //funcion que regresa la accion de la peticion
    function request() {
        return {
            type: userConstants.GET_ALL_USER_REQUEST
        }
    }

    //funcion que regresa la accion de exito con los datos del servidor
    function success(users){
        return {type: userConstants.GET_ALL_USER_REQUEST_SUCCESSS, payload: users}
    }
}