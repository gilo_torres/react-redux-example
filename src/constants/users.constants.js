/**
 * Constantes de peticion api de usuarios
 */
export const userConstants = {
    GET_ALL_USER_REQUEST: 'GET_ALL_USER',
    GET_ALL_USER_REQUEST_SUCCESSS: 'GET_ALL_USER_REQUEST_SUCCESS',
    GET_ALL_USER_REQUEST_FAIL: 'GET_ALL_USER_REQUEST_FAIL'
}