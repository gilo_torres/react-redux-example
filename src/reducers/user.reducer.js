import {userConstants} from '../constants/users.constants'

const initialState = {
    allUser: []
}

/**
 * funcion del reducer
 * siempre recibe como parametros el estado actual y la accion
 */
export default function userReducer(state = initialState, action){
    switch(action.type) {
        case userConstants.GET_ALL_USER_REQUEST:
            return {...state}
        case userConstants.GET_ALL_USER_REQUEST_SUCCESSS:
            return {...state, allUser: action.payload}
        default:
            return {...state}
            
    }
}