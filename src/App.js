import React from 'react';
import './App.css';
import { ListGroup } from 'react-bootstrap';

import { connect } from 'react-redux';

import { usersActions } from './actions/users.actions'

class App extends React.Component {

  constructor(props){
     super(props);

    //Permite utilizar las propiedades del componente
    this.getAllUser = this.getAllUser.bind(this)
  }
  /**
   * funcion que se ejecuta al darle click en el boton de traerte los usuarios.
   */
  getAllUser() {
    this.props.dispatch(usersActions.getAllUser())
  }

  render(){
    const { users } = this.props; //Obtenemos el state de la props del componente
    console.log('Store de redux', users) //Imprimimos el state de redux

    return (
      //JSX
      <div className="App">
        <header className="App-header">
          <button onClick={this.getAllUser}>Traete los usuarios</button>

          <ListGroup>
            {
              //javascript
              users.allUser.map((user, index) => {
                return (
                  //JSX
                  <ListGroup.Item key={index}>{`${user.name} ${user.apellido}`}</ListGroup.Item>
                )
              })
            }
          </ListGroup>
        </header>
      </div>
    );
  };
  
  
}

/**
 * funcion que se utiliza para hacer la conexion y pasar las propiedades del storage a los componentes
 * @param {object} state 
 */
function mapsPropsState(state){
  return {
    users: state.users
  }
}

//se exporta el componente con la conexion al storage de redux
export default connect(mapsPropsState)(App);
