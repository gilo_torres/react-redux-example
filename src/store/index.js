import { createStore, combineReducers, applyMiddleware } from 'redux';

//librerias async/await 
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

import userReducer from '../reducers/user.reducer'

//crear logger middleware
const loggerMiddleware = createLogger()

// conbina reducer para nuestro store
const rootReducers = combineReducers(
    {
        users: userReducer
    }
)

// se crea el Store con todos los reducers
// y se agregan los middleware de log y funciones async/await
export const store = createStore(rootReducers, applyMiddleware(loggerMiddleware, thunkMiddleware));